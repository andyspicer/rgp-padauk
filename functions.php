<?php
/**
 * Functions
 *
 * @package      RGP_Padauk_Child_Theme
 * @since        1.0.0
 * @link         https://awsisme@bitbucket.org/awsisme/rgp-padauk.git
 * @author       Andy Spicer <aspicer@razegroup.com>
 * @copyright    Copyright (c) 2013, Razegroup, llc
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

/**
 * Theme Setup
 * @since 1.0.0
 *
 * This setup function attaches all of the site-wide functions 
 * to the correct hooks and filters. All the functions themselves
 * are defined below this setup function.
 *
 */

add_action('genesis_setup','rgp_rgp_child_theme_setup', 15);
function rgp_child_theme_setup() {

    // ** Backend **


    // Image Sizes
    // Make sure to add the sizes to the dropdown in the function below
    add_image_size( 'rgp-bio-small', 110, 9999 ); //110 pixels wide (and unlimited height)
    add_image_size( 'rgp-bio-large', 300, 9999 ); //300 pixels wide (and unlimited height)

    // Add image choices to media dropdown
    add_filter('image_size_names_choose', 'rgp_image_sizes');


    // Add new UI sections that are included on every page
    //add_action( 'genesis_before_header', 'rgp_add_searchbar');
    //add_action( 'genesis_before_header', 'rgp_add_dataroom');


    /** Reposition the primary navigation menu */
    //remove_action( 'genesis_after_header', 'genesis_do_nav' );
    //add_action( 'genesis_header', 'genesis_do_nav' );

    // Add image to header link
    add_filter( 'genesis_seo_title', 'rgp_header_title', 10, 3 );

    // Genesis Structural Wraps
    add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );

    // Menus
    add_theme_support( 'genesis-menus', array( 'primary' => 'Primary Navigation Menu' ) );

    // Sidebars
    unregister_sidebar( 'sidebar-alt' );
    //genesis_register_sidebar( array( 'name' => 'Blog Sidebar', 'id' => 'blog-sidebar' ) );
    //add_theme_support( 'genesis-footer-widgets', 4 );

    // Remove Unused Page Layouts
    //genesis_unregister_layout( 'full-width-content' );
    //genesis_unregister_layout( 'content-sidebar' );
    genesis_unregister_layout( 'sidebar-content' );
    genesis_unregister_layout( 'content-sidebar-sidebar' );
    genesis_unregister_layout( 'sidebar-sidebar-content' );
    genesis_unregister_layout( 'sidebar-content-sidebar' );

    // Remove Unused User Settings
    add_filter( 'user_contactmethods', 'rgp_contactmethods' );
    remove_action( 'show_user_profile', 'genesis_user_options_fields' );
    remove_action( 'edit_user_profile', 'genesis_user_options_fields' );
    remove_action( 'show_user_profile', 'genesis_user_archive_fields' );
    remove_action( 'edit_user_profile', 'genesis_user_archive_fields' );
    remove_action( 'show_user_profile', 'genesis_user_seo_fields' );
    remove_action( 'edit_user_profile', 'genesis_user_seo_fields' );
    remove_action( 'show_user_profile', 'genesis_user_layout_fields' );
    remove_action( 'edit_user_profile', 'genesis_user_layout_fields' );

    // Editor Styles
    add_editor_style( 'editor-style.css' );

    // Setup Theme Settings
    include_once( CHILD_DIR . '/includes/rgp-theme-settings.php' );

    // Don't update theme
    add_filter( 'http_request_args', 'rgp_dont_update_theme', 5, 2 );

    // ** Frontend **

    // Remove Edit link
    add_filter( 'genesis_edit_post_link', '__return_false' );

    // Responsive Meta Tag
    add_action( 'genesis_meta', 'rgp_viewport_meta_tag' );

    // Footer
    remove_action( 'genesis_footer', 'genesis_do_footer' );
    //add_action( 'genesis_footer', 'rgp_footer' );
}



// ** Backend Functions ** //

/**
 * Customize Contact Methods
 * @since 1.0.0
 *
 * @author Bill Erickson
 * @link http://sillybean.net/2010/01/creating-a-user-directory-part-1-changing-user-contact-fields/
 *
 * @param array $contactmethods
 * @return array
 */
function rgp_contactmethods( $contactmethods ) {
	unset( $contactmethods['aim'] );
	unset( $contactmethods['yim'] );
	unset( $contactmethods['jabber'] );
	
	return $contactmethods;
}

/**
 * Don't Update Theme
 * @since 1.0.0
 *
 * If there is a theme in the repo with the same name, 
 * this prevents WP from prompting an update.
 *
 * @author Mark Jaquith
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r, request arguments
 * @param string $url, request url
 * @return array request arguments
 */

function rgp_dont_update_theme( $r, $url ) {
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
		return $r; // Not a theme update request. Bail immediately.
	$themes = unserialize( $r['body']['themes'] );
	unset( $themes[ get_option( 'template' ) ] );
	unset( $themes[ get_option( 'stylesheet' ) ] );
	$r['body']['themes'] = serialize( $themes );
	return $r;
}

/**
 * Adds newly created image sizes to selection dropdown in the Media Screen
 *
 * @author Andy Spicer
 * @link http://www.andyspicer.com
 *
 * @todo these image sizes should get added to an array when they are created above and the array should get iterated through to add them here.
 */
function rgp_image_sizes($sizes) {
    $addsizes = array(
        "rgp-bio-small" => __( "Bio Small"),
        "rgp-bio-large" => __( "Bio Large")
    );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

// ** Frontend Functions ** //

/**
 * Viewport Meta Tag for Mobile Browsers
 *
 * @author Andy Spicer
 * @link http://www.andyspicer.com
 */
function rgp_viewport_meta_tag() {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
}


/**
 * Sample functions to add UI sections that appear on every page.
 *
 * @author Andy Spicer
 * @link http://www.andyspicer.com
 */

function rgp_add_searchbar() {
    echo '<div id="rgp_searchbar">';
    echo (get_search_form());
    echo '</div>';
}


function rgp_add_dataroom() {
    echo '<div id="rgp_dataroom">';
    echo '<a href="http://dataroom.cetane.net" target="_blank"> <img src="' . get_stylesheet_directory_uri() . '/images/data-room-button.png" alt="Cetane Data Room" class="dataroom_button"/></a>';
    echo '</div>';
}

/**
 * Change default Header URL.
 *
 * @author Andy Spicer
 * @link http://www.razegroup.com
 *
 * @param string $title complete title including $wrap and $inside
 * @param string $inside contents of the $title with link and site name
 * @param string $wrap outer $title tags, may be h1 or h4
 * @return string
 */
function rgp_header_title( $title, $inside, $wrap ) {
    $inside = sprintf( '<a href="' . echo siteurl() . '" title="%s">%s</a>', esc_attr( get_bloginfo( 'name' ) ), get_bloginfo( 'name' ) );
    return sprintf( '<%1$s id="title">%2$s</%1$s>', $wrap, $inside );
}