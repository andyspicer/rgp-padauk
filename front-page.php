<?php

/**
 * Front Page
 *
 * @package      Bubinga
 * @author       Andy Spicer <aspicer@razegroup.com>
 * @copyright    Copyright (c) 2013, Razegroup, LLC
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 *
 * This is a template for a homepage that doesn't have a blog on it.
 *
 */

// Force full width
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Remove Page Title
remove_action( 'genesis_post_title', 'genesis_do_post_title' );

// Content Area
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'rgp_home_loop' );

/**
 * Home Main Content Area
 *
 */

function rgp_home_loop() {
// add content here.

	
}

genesis();
